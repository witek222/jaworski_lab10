package com.itextpdf;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.FileOutputStream;

public class lab10 {
    public static void main(String[] args) {
        Document document = new Document();

        try {
            PdfWriter.getInstance(document,
                    new FileOutputStream("Resume.pdf"));

            document.open();

            Font f1 = new Font(Font.FontFamily.TIMES_ROMAN, 30,Font.BOLD);

            Paragraph p1 = new Paragraph("Resume",f1);
            p1.setAlignment(Element.ALIGN_CENTER);
            p1.setSpacingAfter(50);

            PdfPTable table = new PdfPTable(2);

            PdfPCell cell1 = new PdfPCell(new Paragraph("First Name"));
            PdfPCell cell2 = new PdfPCell(new Paragraph("Witold"));
            PdfPCell cell3 = new PdfPCell(new Paragraph("Last name"));
            PdfPCell cell4 = new PdfPCell(new Paragraph("Jaworski"));
            PdfPCell cell5 = new PdfPCell(new Paragraph("Profession"));
            PdfPCell cell6 = new PdfPCell(new Paragraph("Student"));
            PdfPCell cell7 = new PdfPCell(new Paragraph("Education"));
            PdfPCell cell8 = new PdfPCell(new Paragraph("2017-  PWSZ Tarnów"));
            PdfPCell cell9 = new PdfPCell(new Paragraph("Summary"));
            PdfPCell cell10 = new PdfPCell(new Paragraph("I'm trying to learn advanced www techniques"));

            cell1.setPaddingLeft(10);
            cell2.setPaddingLeft(10);
            cell3.setPaddingLeft(10);
            cell4.setPaddingLeft(10);
            cell5.setPaddingLeft(10);
            cell6.setPaddingLeft(10);
            cell7.setPaddingLeft(10);
            cell8.setPaddingLeft(10);
            cell9.setPaddingLeft(10);
            cell10.setPaddingLeft(10);

            cell1.setPaddingTop(5);
            cell2.setPaddingTop(5);
            cell3.setPaddingTop(5);
            cell4.setPaddingTop(5);
            cell5.setPaddingTop(5);
            cell6.setPaddingTop(5);
            cell7.setPaddingTop(5);
            cell8.setPaddingTop(5);
            cell9.setPaddingTop(5);
            cell10.setPaddingTop(5);

            cell1.setPaddingBottom(5);
            cell2.setPaddingBottom(5);
            cell3.setPaddingBottom(5);
            cell4.setPaddingBottom(5);
            cell5.setPaddingBottom(5);
            cell6.setPaddingBottom(5);
            cell7.setPaddingBottom(5);
            cell8.setPaddingBottom(5);
            cell9.setPaddingBottom(5);
            cell10.setPaddingBottom(5);

            table.addCell(cell1);
            table.addCell(cell2);
            table.addCell(cell3);
            table.addCell(cell4);
            table.addCell(cell5);
            table.addCell(cell6);
            table.addCell(cell7);
            table.addCell(cell8);
            table.addCell(cell9);
            table.addCell(cell10);

            document.add(p1);
            document.add(table);

            document.close();
        } catch(Exception e){

        }
    }
}

